import React from "react";
import ReactDOM from "react-dom/client";

import { prepareMocks } from "./mocks";

import "./index.css";

const root = document.getElementById("root");

prepareMocks().then(() => {
  if (root) {
    ReactDOM.createRoot(root).render(<React.StrictMode>Hello World!</React.StrictMode>);
  }
});
