# Dreamix React Todo App

## Bare bones Template

In this template we expect you to take charge and start from 0. We expect for you to create your own components and build up the folder structures yourself. We expect from you to have decent error handling and design closely resembling the provided demo project. Happy coding!

## Development

Before you run the project for the first time, you need to install all the dependencies. In you terminal run:

```
npm i
```

then

```
npm run dev
```

This command will open your default browser to `localhost:3000` and you can start going from there.

## OR

To run it with docker:

```
docker compose up
```

- Browser won't automatically open if using docker, you need to manually go to `localhost:3000`
- Also make sure port 3000 is free as it is the only exported port
